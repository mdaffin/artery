use nannou::color::{Alpha, Hsl};
use nannou::image::{DynamicImage, ImageBuffer, Rgb};
use nannou::noise::{NoiseFn, Perlin, Seedable};
use nannou::prelude::*;
use nannou::ui::prelude::*;

const PARTICLE_COUNT: usize = 5000;
const PARTICLE_COLOR: f32 = 0.0;
const PARTICLE_SPEED: f32 = 100.0;
const PARTICLE_SIZE: f32 = 5.0;
const PARTICLE_TRAIL: f32 = 0.1;
const NOISE_SCALE: f64 = 0.004;
const DEBUG_DRAW: bool = false;
const PARTICLE_DECAY: f32 = 0.00;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Particle {
    life: f32,
    color: Hsl,
    position: Point2<f32>,
}

fn update(app: &App, model: &mut Model, _update: Update) {
    update_ui(model);

    let win = app.window_rect();
    let delta = app.duration.since_prev_update.as_secs_f32();
    let noise_x = model.noise_x;
    let noise_y = model.noise_y;

    for particle in &mut model.particles {
        let x_delta = noise_x.get([
            model.noise_scale * particle.position.x as f64,
            model.noise_scale * particle.position.y as f64,
        ]) as f32
            * delta
            * model.particle_speed;
        let y_delta = noise_y.get([
            model.noise_scale * particle.position.x as f64,
            model.noise_scale * particle.position.y as f64,
        ]) as f32
            * delta
            * model.particle_speed;

        particle.position += Vector2::new(x_delta, y_delta);
        particle.life -= model.particle_decay * delta;

        if particle.position.x > win.right() + 10.0
            || particle.position.x < win.left() - 10.0
            || particle.position.y > win.top() + 10.0
            || particle.position.y < win.bottom() - 10.0
            || (x_delta.abs() < 0.001 && y_delta.abs() < 0.001)
        {
            particle.life = -1.0;
        }

        if particle.life <= 0.0 {
            particle.life = 1.0;
            match random::<u8>() % 4 {
                0 => {
                    particle.position.x = win.right() + 5.0;
                    particle.position.y = random_range(win.bottom(), win.top());
                }
                1 => {
                    particle.position.x = win.left() - 5.0;
                    particle.position.y = random_range(win.bottom(), win.top());
                }
                2 => {
                    particle.position.y = win.top() + 5.0;
                    particle.position.x = random_range(win.left(), win.right());
                }
                3 => {
                    particle.position.y = win.bottom() - 5.0;
                    particle.position.x = random_range(win.left(), win.right());
                }
                _ => unreachable!(),
            }
        }
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    if app.elapsed_frames() == 0 {
        draw.background().color(BLACK);
    } else {
        let win = app.window_rect();
        draw.rect()
            .w_h(win.w(), win.h())
            .color(srgba(0.0, 0.0, 0.0, model.particle_trail));
    }

    if model.debug_draw {
        draw.texture(&model.noise_texture);
    }

    for particle in &model.particles {
        let (h, s, l) = particle.color.into_components();
        draw.ellipse()
            .w_h(model.particle_size, model.particle_size)
            .xy(particle.position)
            .color(Alpha::<Hsl<_, _>, _>::from_components((
                h,
                s,
                l,
                particle.life,
            )));

        if model.debug_draw {
            draw.line()
                .start(particle.position)
                .end(
                    particle.position
                        + pt2(
                            model.noise_x.get([
                                model.noise_scale * particle.position.x as f64,
                                model.noise_scale * particle.position.y as f64,
                            ]) as f32,
                            model.noise_y.get([
                                model.noise_scale * particle.position.x as f64,
                                model.noise_scale * particle.position.y as f64,
                            ]) as f32,
                        ) * 50.0,
                )
                .weight(2.0)
                .color(STEELBLUE);
        }
    }

    draw.to_frame(app, &frame).unwrap();
    if model.show_ui {
        model.ui.draw_to_frame(app, &frame).unwrap();
    }
}

struct Model {
    _window: window::Id,
    ui: Ui,
    ids: Ids,

    particles: Vec<Particle>,
    noise_texture: wgpu::Texture,
    noise_x: Perlin,
    noise_y: Perlin,

    show_ui: bool,
    particle_decay: f32,
    particle_size: f32,
    particle_speed: f32,
    particle_trail: f32,
    noise_scale: f64,
    debug_draw: bool,
}

widget_ids! {
    struct Ids {
    particle_color,
    particle_count,
    particle_decay,
    particle_size,
    particle_speed,
    particle_trail,
    noise_scale,
    debug_draw,
    }
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .key_pressed(key_pressed)
        .view(view)
        .build()
        .unwrap();

    let win_rect = app.window_rect();

    let particles = (0..PARTICLE_COUNT)
        .map(|_| Particle {
            life: 1.0,
            color: Hsl::new(
                PARTICLE_COLOR,
                random_range(0.0, 1.0),
                random_range(0.7, 0.8),
            ),
            position: Point2::new(
                random_range(win_rect.left(), win_rect.right()),
                random_range(win_rect.bottom(), win_rect.top()),
            ),
        })
        .collect();

    let mut args = std::env::args().skip(1);

    let noise_x = Perlin::new().set_seed(dbg!(args
        .next()
        .map(|arg| arg.parse().unwrap())
        .unwrap_or(random())));
    let noise_y = Perlin::new().set_seed(dbg!(args
        .next()
        .map(|arg| arg.parse().unwrap())
        .unwrap_or(random())));

    let noise_texture = wgpu::Texture::from_image(
        app,
        &DynamicImage::ImageRgb8(ImageBuffer::from_fn(
            win_rect.w() as u32,
            win_rect.h() as u32,
            |x, y| {
                let value_x = noise_x.get([NOISE_SCALE * x as f64, NOISE_SCALE * y as f64]);
                let value_y = noise_y.get([NOISE_SCALE * x as f64, NOISE_SCALE * y as f64]);
                let value_x = ((value_x + 1.0) / 2.0 * 255.0) as u8;
                let value_y = ((value_y + 1.0) / 2.0 * 255.0) as u8;
                Rgb([value_x, value_y, 0])
            },
        )),
    );

    let mut ui = app.new_ui().build().unwrap();
    let ids = Ids::new(ui.widget_id_generator());

    Model {
        _window,
        ui,
        ids,

        particles,
        noise_x,
        noise_y,
        noise_texture,

        show_ui: true,
        particle_speed: PARTICLE_SPEED,
        particle_size: PARTICLE_SIZE,
        particle_trail: PARTICLE_TRAIL,
        particle_decay: PARTICLE_DECAY,
        noise_scale: NOISE_SCALE,
        debug_draw: DEBUG_DRAW,
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

fn update_ui(model: &mut Model) {
    if model.show_ui {
        let ui = &mut model.ui.set_widgets();

        macro_rules! slider {
            ($label:expr, $param:ident, $type:ty, min:$min:expr, max:$max:expr, top:$top:expr) => {
                let slider = slider!(@ $label, $param, $min, $max).top_left_with_margin($top);
                slider!(@ $param, $type, slider);
            };
            ($label:expr, $param:ident, $type:ty, min:$min:expr, max:$max:expr, down:$down:expr) => {
                let slider = slider!(@ $label, $param, $min, $max).down($down);
                slider!(@ $param, $type, slider);
            };
            (@ $param:ident, $type:ty, $slider:expr) => {
                if let Some(value) = $slider.set(model.ids.$param, ui) {
                    model.$param = value as $type;
                }
            };

            (@ $label:expr, $param:ident, $min:expr, $max:expr) => {
                widget::Slider::new(model.$param as f32, $min, $max)
                    .w_h(200.0, 30.0)
                    .label_font_size(15)
                    .rgb(0.3, 0.3, 0.3)
                    .label_rgb(1.0, 1.0, 1.0)
                    .border(0.0)
                    .label($label)
            };
        }

        slider!("Particle Speed", particle_speed, f32, min:0.0, max:1000.0, top:20.0);
        slider!("Particle Size", particle_size, f32, min:1.0, max:10.0, down:10.0);
        slider!("Particle trail", particle_trail, f32, min:0.0, max:1.0, down:10.0);
        slider!("Particle Decay", particle_decay, f32, min:0.0, max:1.0, down:10.0);
        slider!("Noise Scale", noise_scale, f64, min:0.00001, max:0.01, down:10.0);
        let toggle = widget::Toggle::new(model.debug_draw)
            .w_h(200.0, 30.0)
            .label_font_size(15)
            .rgb(0.3, 0.3, 0.3)
            .label_rgb(1.0, 1.0, 1.0)
            .border(0.0)
            .label("Draw Debug Visuals")
            .down(20.0)
            .set(model.ids.debug_draw, ui);
        if toggle.count() % 2 == 1 {
            model.debug_draw = !model.debug_draw
        };
    }
}

fn key_pressed(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::Space => {
            model.show_ui = !model.show_ui;
        }
        _ => {}
    }
}
