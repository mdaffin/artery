use nannou::prelude::*;
use nannou::ui::prelude::*;
use std::f32::consts::TAU;

fn main() {
    nannou::app(model).update(update).run();
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let win = app.window_rect();
    let point_spacing = win.w() / (model.number_of_points as f32 - 1.0) as f32;

    let wave = Wave::new(model.amplitude, model.period, app.time * model.speed);

    if app.elapsed_frames() == 0 {
        draw.background().color(BLACK);
    } else {
        draw.rect()
            .w_h(win.w(), win.h())
            .color(srgba(0.0, 0.0, 0.0, model.trail));
    }

    for i in 0..model.number_of_points {
        let i = i as f32;

        let x = point_spacing * i + win.left();
        let y = wave.y_for(x);
        let x_next = point_spacing * (i + 1.0) + win.left();
        let y_next = wave.y_for(x_next);

        draw.line()
            .weight(1.0)
            .caps_round()
            .color(rgba(0.0, 1.0, 0.0, 0.2))
            .points(Vector2::new(x, 0.0), Vector2::new(x, y));

        draw.line()
            .weight(model.point_size)
            .caps_round()
            .color(rgba(0.0, 1.0, 1.0, 1.0))
            .points(Vector2::new(x, y), Vector2::new(x_next, y_next));

        /*
        draw.ellipse()
            .x_y(x, y)
            .radius(model.point_size)
            .color(rgba(0.0, 1.0, 1.0, 1.0));
            */
    }

    draw.to_frame(app, &frame).unwrap();
    model.ui.draw_to_frame(app, &frame).unwrap();
}

struct Wave {
    amplitude: f32,
    period: f32,
    phase: f32,
}

impl Wave {
    fn new(amplitude: f32, period: f32, phase: f32) -> Self {
        Self {
            amplitude,
            period,
            phase,
        }
    }

    fn y_for(&self, x: f32) -> f32 {
        f32::sin(self.phase + TAU + x / self.period) * self.amplitude
    }
}

struct Model {
    ui: Ui,
    ids: Ids,
    number_of_points: u32,
    amplitude: f32,
    period: f32,
    speed: f32,
    trail: f32,
    point_size: f32,
    show_ui: bool,
}

widget_ids! {
    struct Ids {
        number_of_points,
        amplitude,
        period,
        speed,
        trail,
        point_size,
        show_ui,
    }
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .key_pressed(key_pressed)
        .size(800, 600)
        .title("Sine Wave")
        .view(view)
        .build()
        .unwrap();
    let mut ui = app.new_ui().build().unwrap();
    let ids = Ids::new(ui.widget_id_generator());
    let point_size = 4.0;

    Model {
        ui,
        ids,
        number_of_points: 100,
        amplitude: max_amplitude(point_size, app),
        period: (app.window_rect().w() - point_size * 2.0) / TAU,
        speed: 1.0,
        trail: 0.05,
        point_size,
        show_ui: false,
    }
}

fn max_amplitude(point_size: f32, app: &App) -> f32 {
    (app.window_rect().h() - point_size * 2.0) / 2.0
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let ui = &mut model.ui.set_widgets();
    if model.show_ui {
        fn slider(val: f32, min: f32, max: f32) -> widget::Slider<'static, f32> {
            widget::Slider::new(val, min, max)
                .w_h(200.0, 30.0)
                .label_font_size(15)
                .rgb(0.3, 0.3, 0.3)
                .label_rgb(1.0, 1.0, 1.0)
                .border(0.0)
        }

        if let Some(value) = slider(model.number_of_points as f32, 10.0, 200.0)
            .top_left_with_margin(20.0)
            .label("Number of Points")
            .set(model.ids.number_of_points, ui)
        {
            model.number_of_points = value as u32;
        }

        if let Some(value) = slider(model.amplitude, 0.0, max_amplitude(model.point_size, app))
            .down(10.0)
            .label("Amplitude")
            .set(model.ids.amplitude, ui)
        {
            model.amplitude = value;
        }

        if let Some(value) = slider(model.period, 0.01, 600.0)
            .down(10.0)
            .label("Period")
            .set(model.ids.period, ui)
        {
            model.period = value;
        }

        if let Some(value) = slider(model.speed, 0.0, 10.0)
            .down(10.0)
            .label("Speed")
            .set(model.ids.speed, ui)
        {
            model.speed = value;
        }

        if let Some(value) = slider(1.0 - model.trail, 0.00, 1.0)
            .down(10.0)
            .label("Trail")
            .set(model.ids.trail, ui)
        {
            model.trail = 1.0 - value;
        }

        if let Some(value) = slider(model.point_size, 0.0, 10.0)
            .down(10.0)
            .label("Point Size")
            .set(model.ids.point_size, ui)
        {
            model.point_size = value;
        }
    }
}

fn key_pressed(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::Space => {
            model.show_ui = !model.show_ui;
        }
        _ => {}
    }
}
