use nannou::prelude::*;

const BOID_COUNT: u32 = 200;
const BOID_FOV_RADIUS: f32 = 25.0;
const BOID_SPEED: f32 = 240.0;
const BOID_TURNING_WEIGHT: f32 = 0.5;
const SEPERATION_WEIGHT: f32 = 6.0;
const COHESION_WEIGHT: f32 = 1.0;
const ALIGNMENT_WEIGHT: f32 = 0.01;
const RANDOM_WEIGHT: f32 = 10.0;
const TRAIL: f32 = 0.1;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Boid {
    position: Point2<f32>,
    velocity: Vector2<f32>,
}

fn cohesion(boid: Boid, boids: &[Boid]) -> Vector2<f32> {
    assert!(boids.len() > 0, "no local boids");
    let center = boids.iter().map(|boid| boid.position).sum::<Point2>() / boids.len() as f32;
    center - boid.position
}

fn alignment(boids: &[Boid]) -> Vector2<f32> {
    assert!(boids.len() > 0, "no local boids");
    boids.iter().map(|boid| boid.velocity).sum::<Point2>() / boids.len() as f32
}

fn separation(boid: Boid, boids: &[Boid]) -> Vector2<f32> {
    assert!(boids.len() > 0, "no local boids");
    boids
        .iter()
        .map(|local_boid| local_boid.position - boid.position)
        .map(|distance| distance - distance.normalize() * BOID_FOV_RADIUS)
        .sum::<Point2>()
        / boids.len() as f32
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let delta = app.duration.since_prev_update.as_secs_f32();
    let win = app.window_rect();

    model.boids = model
        .boids
        .iter()
        .map(|&boid| {
            let local_boids = local_boids(boid, &model.boids).collect::<Vec<_>>();

            let target_vel = if local_boids.len() > 0 {
                separation(boid, &local_boids) * SEPERATION_WEIGHT
                    + cohesion(boid, &local_boids) * COHESION_WEIGHT
                    + alignment(&local_boids) * ALIGNMENT_WEIGHT
                    + Vector2::new(random_f32(), random_f32()).normalize() * RANDOM_WEIGHT
            } else {
                boid.velocity
            };

            let v = boid.velocity;
            Boid {
                position: boid.position,
                velocity: ((target_vel - v) * BOID_TURNING_WEIGHT + v).normalize() * BOID_SPEED,
            }
        })
        .collect();

    for boid in &mut model.boids {
        boid.position += boid.velocity * delta;

        if boid.position.x > win.right() {
            boid.position.x -= win.w();
        }
        if boid.position.x < win.left() {
            boid.position.x += win.w();
        }
        if boid.position.y > win.top() {
            boid.position.y -= win.h();
        }
        if boid.position.y < win.bottom() {
            boid.position.y += win.h();
        }
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    if app.elapsed_frames() == 0 {
        draw.background().color(BLACK);
    } else {
        let win = app.window_rect();
        draw.rect()
            .w_h(win.w(), win.h())
            .color(srgba(0.0, 0.0, 0.0, TRAIL));
    }

    for boid in &model.boids {
        draw.ellipse()
            .w_h(10.0, 10.0)
            .xy(boid.position)
            .color(WHITE);
    }

    draw.to_frame(app, &frame).unwrap();
}

fn local_boids(boid: Boid, boids: &[Boid]) -> impl Iterator<Item = Boid> + '_ {
    boids
        .iter()
        .cloned()
        .filter(move |&other_boid| other_boid != boid)
        .filter(move |other_boid| {
            (boid.position - other_boid.position).magnitude2() < BOID_FOV_RADIUS * BOID_FOV_RADIUS
        })
}

struct Model {
    _window: window::Id,
    boids: Vec<Boid>,
}

fn model(app: &App) -> Model {
    let _window = app.new_window().view(view).build().unwrap();

    let win_rect = app.window_rect();

    let boids = (0..BOID_COUNT)
        .map(|_| Boid {
            position: Point2::new(
                random_range(win_rect.left(), win_rect.right()),
                random_range(win_rect.bottom(), win_rect.top()),
            ),
            velocity: Vector2::new(random_range(-1.0, 1.0), random_range(-1.0, 1.0)).normalize()
                * BOID_SPEED,
        })
        .collect();
    Model { _window, boids }
}

fn main() {
    nannou::app(model).update(update).run();
}
